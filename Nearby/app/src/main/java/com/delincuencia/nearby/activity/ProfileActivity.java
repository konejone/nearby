package com.delincuencia.nearby.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delincuencia.nearby.R;
import com.delincuencia.nearby.util.SharedPreferenceHelper;

import org.w3c.dom.Text;


/**
 * Created by ricardo.gutierrezc on 05/12/2016.
 */

public class ProfileActivity extends Fragment {
    TextView nombre;
    TextView apellido;
    TextView sexo;
    TextView direccion;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inicializaElementos();
        rellenaInformacion();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_activity,container,false);
    }



    public void inicializaElementos(){
        this.nombre = (TextView) getView().findViewById(R.id.nombre);
        this.apellido = (TextView) getView().findViewById(R.id.apellido);
        this.sexo = (TextView) getView().findViewById(R.id.sexo);
        this.direccion = (TextView) getView().findViewById(R.id.direccion);
    }
    public void rellenaInformacion(){
        this.nombre.setText(" "+SharedPreferenceHelper.getFirstname(this.getContext()));
        this.apellido.setText(" "+SharedPreferenceHelper.getLastname(this.getContext()));
        this.sexo.setText(" "+SharedPreferenceHelper.getSex(this.getContext()));
        this.direccion.setText(" "+SharedPreferenceHelper.getAddress(this.getContext()));
    }
}
